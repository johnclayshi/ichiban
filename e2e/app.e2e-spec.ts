import { IchibanPage } from './app.po';

describe('ichiban App', () => {
  let page: IchibanPage;

  beforeEach(() => {
    page = new IchibanPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
